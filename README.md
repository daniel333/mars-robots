# mars-robots

A code challenge written in Typescript.

It doesn't need to be compiled due to it will be run with `ts-node`.

## Quick start

To run the current project you should have installed Node > 8, being preferred the last Node 10.

To install it:

```bash
git clone git@gitlab.com:daniel333/mars-robots.git
cd mars-robots
npm ci # or "npm install"
```

And finally to run it:

```bash
npm start # CLI

npm run sample # By default it runs the default "Sample Input" from the PDF
```

## Testing

Run the next command in your terminal:

```bash
npm test
```
