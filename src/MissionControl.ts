import {
  Dimension,
  Orientation,
  RobotAction,
  areDimensionsEqual
} from './Types'
import { Robot } from './Robot'
import { World } from './World'

// Array of hooks to run when the robot is gonna perform its buffer of instructions
// Each hook will return "true" if the instruction should be ignored
const cannotPerformHooks: ((
  mc: MissionControl,
  robot: Robot,
  action: RobotAction
) => boolean)[] = [
  // If the robot is 'LOST' then ignore the action
  function ignoreInstructionIfRobotIsLost(
    mc: MissionControl,
    robot: Robot,
    action: RobotAction
  ) {
    return robot.isLost()
  },
  // This hook will check if the next action will do the robot to be 'LOST'
  function ignoreActionIfSmellsRobotLost(
    mc: MissionControl,
    robot: Robot,
    action: RobotAction
  ) {
    if (action === RobotAction.F) {
      return mc.robots.some(
        r =>
          r !== robot &&
          r.isLost() &&
          r.getOrientation() === robot.getOrientation() &&
          areDimensionsEqual(r.getPosition(), robot.getPosition())
      )
    }
    return false
  }
]

export class MissionControl {
  private world: World
  robots: Robot[] = []

  constructor(props: { world: Dimension }) {
    const { world } = props
    this.world = new World(world)
  }

  // Robot Factory
  deployRobot(props: {
    position: Dimension
    orientation: keyof typeof Orientation
  }) {
    const robot = new Robot({ ...props, world: this.world })
    this.robots.push(robot)
    return robot
  }

  performRobotActions(robot: Robot, actions: RobotAction[]) {
    // exec actions
    actions.forEach(action => {
      // Checks if every hook allows the execution of the instructions
      const cannotPerform = cannotPerformHooks.some(
        hook => hook(this, robot, action) === true
      )

      if (!cannotPerform) {
        robot.performAction(action)
      }
    })
  }
}
