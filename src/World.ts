import { Dimension } from './Types'

export class World {
  private area: Dimension

  constructor(props: Dimension) {
    this.area = props
  }

  isInside(position: Dimension) {
    return (
      position.x <= this.area.x &&
      position.y <= this.area.y &&
      position.y >= 0 &&
      position.x >= 0
    )
  }

  isOutside(position: Dimension) {
    return !this.isInside(position)
  }
}
