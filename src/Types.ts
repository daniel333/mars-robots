export type Dimension = {
  x: number
  y: number
}

export function areDimensionsEqual(d1: Dimension, d2: Dimension) {
  const keys = Object.keys(d1) as Array<keyof Dimension>
  return keys.every(d => d1[d] === d2[d])
}

// Sorted to help with the rotations
export const Orientation = {
  N: 0,
  E: 1,
  S: 2,
  W: 3
}

export enum RobotStatus {
  'LOST' = 'LOST',
  'ONLINE' = 'ONLINE'
}

export enum RobotAction {
  L = 'L',
  R = 'R',
  F = 'F'
}
