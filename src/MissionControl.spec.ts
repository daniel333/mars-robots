import { MissionControl } from './MissionControl'
import { RobotAction } from './Types'

describe('MissionControl', () => {
  const world = { x: 5, y: 3 }
  const mc = new MissionControl({ world })

  it('robot + instructions #1', () => {
    const position = { x: 1, y: 1 }
    const orientation = 'E'
    const robotActions = 'RFRFRFRF'.split('') as RobotAction[]
    const robot = mc.deployRobot({ position, orientation })

    mc.performRobotActions(robot, robotActions)

    expect(robot.getOrientation()).toBe('E')
    expect(robot.isLost()).toBeFalsy()
    expect(robot.getPosition()).toStrictEqual({ x: 1, y: 1 })
    expect(robot.toString()).toBe('11E')
  })

  it('robot + instructions #2', () => {
    const position = { x: 3, y: 2 }
    const orientation = 'N'
    const robotActions = 'FRRFLLFFRRFLL'.split('') as RobotAction[]
    const robot = mc.deployRobot({ position, orientation })

    mc.performRobotActions(robot, robotActions)

    expect(robot.getOrientation()).toBe('N')
    expect(robot.isLost()).toBeTruthy()
    expect(robot.getPosition()).toStrictEqual({ x: 3, y: 3 })
    expect(robot.toString()).toBe('33NLOST')
  })

  it('robot + instructions #3', () => {
    const position = { x: 0, y: 3 }
    const orientation = 'W'
    const robotActions = 'LLFFFLFLFL'.split('') as RobotAction[]
    const robot = mc.deployRobot({ position, orientation })

    mc.performRobotActions(robot, robotActions)

    expect(robot.getOrientation()).toBe('S')
    expect(robot.isLost()).toBeFalsy()
    expect(robot.getPosition()).toStrictEqual({ x: 2, y: 3 })
    expect(robot.toString()).toBe('23S')
  })
})
