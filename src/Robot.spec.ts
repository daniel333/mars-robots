import { World } from './World'
import { Robot } from './Robot'
import { RobotAction } from './Types'

const INITIAL_ROBOT_POSITION = {
  x: 1,
  y: 1
}

const INITIAL_ROBOT_ORIENTATION = 'E'

describe('Robot', () => {
  const world = new World({ x: 5, y: 4 })
  let robot: Robot | null = null

  beforeEach(() => {
    robot = new Robot({
      position: INITIAL_ROBOT_POSITION,
      orientation: INITIAL_ROBOT_ORIENTATION,
      world
    })
  })

  it('should change its rotation to W after rotate Right twice (180deg)', () => {
    robot?.performAction(RobotAction.R)
    robot?.performAction(RobotAction.R)
    expect(robot?.getOrientation()).toBe('W')
  })

  it('should be lost after a few movements', () => {
    robot?.performAction(RobotAction.R)
    robot?.performAction(RobotAction.F)
    robot?.performAction(RobotAction.F)
    expect(robot?.getPosition()).toStrictEqual({ x: 1, y: 0 })
    expect(robot?.getOrientation()).toBe('S')
    expect(robot?.isLost()).toBeTruthy()
  })

  it('should match toString output with initial position', () => {
    expect(robot?.toString()).toBe('11E')
  })
})
