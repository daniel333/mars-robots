import { MissionControl } from './MissionControl'
import { RobotAction } from './Types'

const world = {
  x: 5,
  y: 3
}

const mc = new MissionControl({ world })

const nRobot = mc.deployRobot({ position: { x: 1, y: 1 }, orientation: 'E' })
mc.performRobotActions(nRobot, 'RFRFRFRF'.split('') as RobotAction[])
console.log(nRobot.toString())

const xRobot = mc.deployRobot({ position: { x: 3, y: 2 }, orientation: 'N' })
mc.performRobotActions(xRobot, 'FRRFLLFFRRFLL'.split('') as RobotAction[])
console.log(xRobot.toString())

const hRobot = mc.deployRobot({ position: { x: 0, y: 3 }, orientation: 'W' })
mc.performRobotActions(hRobot, 'LLFFFLFLFL'.split('') as RobotAction[])
console.log(hRobot.toString())
