import inquirer from 'inquirer'
import { MissionControl } from './MissionControl'

async function main() {
  let missionControl

  const answer = await inquirer.prompt({
    type: 'input',
    name: 'world',
    message: 'Please, enter world dimensions?',
    validate: function(value) {
      var valid = !isNaN(parseFloat(value)) && /^\d{2}$/.test(value)
      return valid || 'Please enter a number'
    }
  })

  const [x, y] = answer.world.split('')
  missionControl = new MissionControl({ world: { x, y } })

  await createNewRobot(missionControl)

  let keepAsking = true

  do {
    const { moreRobots } = await askForMoreRobots()
    if (moreRobots === 'y') {
      await createNewRobot(missionControl)
    } else {
      keepAsking = false
    }
  } while (keepAsking)
}

async function createNewRobot(missionControl: MissionControl) {
  const questions = [
    {
      type: 'input',
      name: 'robotCoordinates',
      message: 'Please, enter robot initial coordinates',
      validate: function(value: any) {
        var valid = /^\d{2}[NWSE]{1}$/.test(value)
        return valid || 'Please enter a number and orientation, i.e.: 11E'
      }
    },
    {
      type: 'input',
      name: 'robotInstructions',
      message: 'Please, enter robot instructions',
      validate: function(value: any) {
        var valid = /^[RLF]+$/g.test(value)
        return valid || 'Please enter a correct input, i.e.: RLFLF'
      }
    }
  ]
  const r1 = await inquirer.prompt(questions)
  const { robotCoordinates, robotInstructions } = r1 as any
  const rbt1 = missionControl.deployRobot(
    parseRobotCoordinatesInput(robotCoordinates)
  )
  missionControl.performRobotActions(rbt1, robotInstructions.split(''))
  console.log(rbt1.toString())
}

function parseRobotCoordinatesInput(input: string) {
  const inputMatch = input.match(/^(\d{2})([NWSE]{1})$/)

  if (!inputMatch) throw new Error('Error parsing initial properties')

  const [x, y] = inputMatch[1].split('')
  return {
    position: { x: parseInt(x), y: parseInt(y) },
    orientation: inputMatch[2]
  } as any
}

function askForMoreRobots() {
  return inquirer.prompt({
    type: 'input',
    name: 'moreRobots',
    message: 'Do you want to create more robots? (y/n)',
    validate: function(value) {
      var valid = /(y|n)/.test(value)
      return valid || 'Please enter a y/n'
    }
  })
}

main()
