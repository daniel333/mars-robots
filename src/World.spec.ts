import { World } from './World'

describe('World', () => {
  let world: World | null = null
  it('should instance a World', () => {
    const dimensions = {
      x: 10,
      y: 10
    }
    world = new World(dimensions)
    expect(world).toBeInstanceOf(World)
  })

  it('should detect a position as outside the grid #1', () => {
    const dimension = {
      x: 11,
      y: 10
    }
    expect(world?.isOutside(dimension)).toBeTruthy()
  })

  it('should detect a position as outside the grid #2', () => {
    const dimension = {
      x: -1,
      y: 1
    }
    expect(world?.isOutside(dimension)).toBeTruthy()
  })

  it('should detect a position as inside the grid #1', () => {
    const dimension = {
      x: 10,
      y: 10
    }
    expect(world?.isInside(dimension)).toBeTruthy()
  })

  it('should detect a position as inside the grid #2', () => {
    const dimension = {
      x: 5,
      y: 0
    }
    expect(world?.isInside(dimension)).toBeTruthy()
  })
})
