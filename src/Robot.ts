import { Dimension, Orientation, RobotStatus, RobotAction } from './Types'
import { World } from './World'

export class Robot {
  private world: World // A Robot should know the world where is deployed
  private previousPosition: Dimension
  private currentPosition: Dimension
  private orientation: keyof typeof Orientation
  private status: RobotStatus = RobotStatus.ONLINE

  constructor(props: {
    position: Dimension
    orientation: keyof typeof Orientation
    world: World
  }) {
    this.world = props.world
    this.currentPosition = this.previousPosition = props.position
    this.orientation = props.orientation
  }

  performAction(action: RobotAction) {
    switch (action) {
      case RobotAction.F:
        this.moveForward()
        break
      case RobotAction.L:
      case RobotAction.R:
        this.changeOrientation(action)
        break
    }
  }

  isLost() {
    return this.status === RobotStatus.LOST
  }

  getPosition() {
    return this.isLost() ? this.previousPosition : this.currentPosition
  }

  getOrientation() {
    return this.orientation
  }

  toString() {
    const status = this.isLost() ? this.status : '' // Should not print the status of the robot if it's alive
    return `${Object.values(this.getPosition()).join('')}${
      this.orientation
    }${status}`
  }

  private moveForward() {
    this.previousPosition = this.currentPosition

    const { x, y } = this.currentPosition
    switch (this.orientation) {
      case 'E':
        this.currentPosition = preventPositionGreaterThan50({ x: x + 1, y })
        break
      case 'N':
        this.currentPosition = preventPositionGreaterThan50({ x, y: y + 1 })
        break
      case 'S':
        this.currentPosition = preventPositionGreaterThan50({ x, y: y - 1 })
        break
      case 'W':
        this.currentPosition = preventPositionGreaterThan50({ x: x - 1, y })
        break
    }

    // checks if the current position is inside area
    if (this.world.isOutside(this.currentPosition)) {
      this.setStatus(RobotStatus.LOST)
    }
  }

  private changeOrientation(action: RobotAction) {
    this.orientation = computeRotation(this.orientation, action)
  }

  private setStatus(status: RobotStatus) {
    this.status = status
  }
}

function preventPositionGreaterThan50(dimension: Dimension) {
  let { x, y } = dimension
  if (x > 50) {
    x = 50
  }
  if (y > 50) {
    y = 50
  }
  return { x, y }
}

// Helper to calc the rotations
function computeRotation(
  currentOrientation: keyof typeof Orientation,
  rotation: RobotAction
) {
  const orientationIndexLength = Object.keys(Orientation).length
  let orientationValue = Orientation[currentOrientation]

  if (rotation === 'L') {
    orientationValue = orientationValue - 1
    // if the value is lesser than 0 then is W (and orientation input was N)
    if (orientationValue < 0) {
      orientationValue = orientationIndexLength - 1
    }
  }

  if (rotation === 'R') {
    orientationValue = orientationValue + 1
    // if the value is greater than 3 then is N (and orientation input was W)
    if (orientationValue >= orientationIndexLength) {
      orientationValue = 0
    }
  }
  return Object.keys(Orientation)[orientationValue] as keyof typeof Orientation
}
